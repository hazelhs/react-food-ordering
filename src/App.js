import React, { Component } from 'react';
import './App.css';
import Navbar from './components/Navbar';
import {Switch, Route} from 'react-router-dom';
import AddFood from './components/manageFood/AddFood';
import Menu from './components/Menu';
import ManageFood from './components/manageFood/ManageFood';
import EditFood from './components/manageFood/EditFood';

import {ToastContainer} from 'react-toastify';
import  'react-toastify/dist/ReactToastify.css';
import Order from './components/Order';
import Home from './components/Home';
import Checkout from "./components/checkout/Checkout";
import cartService from "./service/foodCartService";
import CheckoutSuccess from "./components/CheckoutSuccess";
import OrderPlaced from "./components/manageOrder/OrderPlaced";


class App extends Component {

    state = {
        totalItems: 0,
        orders: []
    };


    async componentDidMount() {

        const result = await cartService.getItemsInCart();
        console.log('result ', result);

        if(result) this.setState({orders: result.data.items});


        const totalItems = await cartService.getCartTotalItem();
        this.setState({totalItems});
    }

    addToCart = async (menu) => {
        // console.log("menu page - add order trigger ", menu);
        const result = await cartService.addToCart(menu);
        this.setState({orders: result.data.items});

        await this.getTotalItems();
    }

    async getTotalItems() {
        const totalItems = await cartService.getCartTotalItem();
        this.setState({totalItems});
    }


    handleDecrease = async (menu) => {
        console.log("menu to decrease ", menu);

        const orders =[...this.state.orders];
        // orders.forEach(order => console.log(order));
        // const index = orders.indexOf(menu);

        const target = orders.filter(o => o._id === menu._id);
        const index = orders.indexOf(target[0]);
        console.log(index);

        if(index > -1) {
            orders[index].quantity--;

            const result = await cartService.updateOrder(localStorage.getItem('cart-id'), orders);

            let totalItems = this.state.totalItems;
            totalItems -= 1;
            this.setState({orders, totalItems});
        } else {
            console.log('cannot find item')
        }

    };

    handleIncrease = async (menu) => {

        console.log("menu to increase ", menu);

        const orders =[...this.state.orders];
        // const index = orders.indexOf(menu);
        // orders[index].quantity++;

        const target = orders.filter(o => o._id === menu._id);
        const index = orders.indexOf(target[0]);
        console.log(index);

        if(index > -1) {
            orders[index].quantity++;

            const result = await cartService.updateOrder(localStorage.getItem('cart-id'), orders);

            let totalItems = this.state.totalItems;
            totalItems += 1;
            this.setState({orders, totalItems});
        } else {
            console.log('cannot find item')
        }
    }


    deleteMenu = async (menu) => {
        console.log("menu to delete ", menu);

        const orders =[...this.state.orders];
        const index = orders.indexOf(menu);
        orders.splice(index, 1);

        const result = await cartService.updateOrder(localStorage.getItem('cart-id'), orders);
        console.log('after delete ', result);

        const totalItems = await cartService.calcTotalItemsInCart(result);
        this.setState({ orders: result.data.items, totalItems });
    }



    render() {

        return (
            <div >
                <Navbar totalItems={this.state.totalItems}/>

                <ToastContainer />

                <div className="container">
                    <div className="row">
                        <div className="col-lg-1 col-md-1"></div>

                        <Switch>

                            <Route path="/manageOrder" component={OrderPlaced} />

                            <Route path="/checkoutSuccess" component={CheckoutSuccess} />

                            <Route path="/checkout" component={Checkout} />

                            {/*<Route path="/orders" component={Order} />*/}

                            <Route path="/reviewOrder"
                                   render={ props =>
                                       <Order {...props}
                                              totalItems={this.state.totalItems}
                                              orders={this.state.orders}
                                              onDeleteOrder={this.deleteMenu}
                                              onIncreaseOrder={this.handleIncrease}
                                              onDecreaseOrder={this.handleDecrease} /> }/>

                            <Route path="/manageMenu/add" component={AddFood} />

                            <Route path="/manageMenu/:id" component={EditFood} />

                            <Route path="/manageMenu" render={ props => <ManageFood {...props} /> } />

                            {/* <Route path="/menu" component={Menu} onClick={this.handleOrder} /> */}

                            <Route path="/menu"
                                   render={ props =>
                                       <Menu {...props}
                                             totalItems={this.state.totalItems}
                                             orders={this.state.orders}
                                             onAddOrder={this.addToCart} />  }/>

                            {/*<Route path="/register" component={Register} />*/}

                            <Route from="/" exact component={Home}/>

                        </Switch>
                    </div>


                </div>
            </div>
        );
    }
}

export default App;
