import React from 'react';

const Home = () => {
    return (

        <div className="row">
            <div className="col-lg-2 col-md-1"></div>
            <div className="col-lg-8 col-md-10">
                <h2 className="home-title">About this project ...</h2>
                <div className="card border-0 shadow mb-4">
                    <div className="card-body">
                        <h5 className="m-0">Description</h5>
                        <hr/>
                        The purpose of building this project is to learn the basic of
                        React, NodeJs and MongoDB for building a website.
                        I have been learning React and NodeJs recently
                        through an online course with Mosh Hamedani
                        <a href="https://codewithmosh.com/"> https://codewithmosh.com/</a>.
                    </div>
                </div>

                <div className="card border-0 shadow mb-4">
                    <div className="card-body">
                        <h5 className="m-0">Developed with</h5>
                        <hr/>
                        <ul>
                            <li><strong>ReactJS</strong></li>
                            <li><strong>NodeJS</strong></li>
                            <li><strong>ExpressJS</strong></li>
                            <li><strong>MongoDB</strong></li>
                            <li><strong>Bootstrap 4</strong> for responsive design</li>
                            <li><strong>Gitlab</strong> to manage code</li>
                            <li><strong>Surge</strong> to publish and host application</li>
                            <li><strong>Amazon EC2</strong> to host NodeJS application and MongoDB</li>
                        </ul>
                    </div>
                </div>

                <div className="card border-0 shadow mb-4">
                    <div className="card-body">
                        <h5 className="m-0">Features</h5>
                        <hr/>
                        <ul className="mb-0">
                            <li>Display a list of food menus that retrieving from database</li>
                            <li>Filtering menus by categories</li>
                            <li>Use of React Router for navigation between pages</li>
                            <li>Add new menu to database</li>
                            <li>Delete existing menu</li>
                            <li>Edit/Update existing menu</li>
                            <li>Add menu to cart</li>
                            <li>Input Form Validation using Joi</li>
                            <li>Total items added is shown on navigation bar</li>
                            <li>Order details page to review menus that has been added</li>
                            <li>Manipulation of order - increase/decrease quantity and delete from cart</li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    );
};

export default Home;
