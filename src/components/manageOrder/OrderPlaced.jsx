import React, {Component} from 'react';
import orderService from "../../service/orderService";
import TableHeader from "../../common/TableHeader";

class OrderPlaced extends Component {

    state = {
        orderDetails: [],
        totalOrders: ''
    };

    async componentDidMount() {
        const {data} = await orderService.getOrders();

        this.setState({totalOrders: data.length});
        this.setState({orderDetails: data});
    }

    render() {

        const {location} = this.props;
        const {orderDetails, totalOrders} = this.state;

        return (
            <div className="col-lg-10 col-md-10">

                <div className="card">
                    <TableHeader
                        isAddBtnExist={false}
                        currentItems={orderDetails.length}
                        totalItems={totalOrders}
                        currentPath={location.pathname}/>

                    <div className="card-body">
                        <table className="table-responsive-md table table-bordered table-striped col" >
                            <thead>
                            <tr>
                                <th width="20%">Order Date</th>
                                <th width="20%">Name</th>
                                <th width="20%">Email</th>
                                <th width="20%">Total Price</th>
                                <th width="20%">Address</th>
                            </tr>
                            </thead>

                            <tbody>
                            {this.renderOrders()}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }

    convertDate(oriDate) {
        console.log('iso date ', oriDate);
        console.log('js date ', new Date(oriDate));
        return new Date(oriDate).toLocaleString();
    }


    renderOrders() {
        return (
            this.state.orderDetails.length > 0
                ? this.state.orderDetails.map(order => (
                    <tr key={order._id}>
                        <td>{this.convertDate(order.datePlaced)}</td>
                        <td>{order.shipping.firstName} {order.shipping.lastName}</td>
                        <td>{order.shipping.email}</td>
                        <td>${order.items.totalPrice}</td>
                        <td>
                            {order.shipping.address.address1}, {order.shipping.address.address2}
                        </td>
                    </tr>
                ))
                : <tr>
                    <td colspan="5" style={{'textAlign': 'center'}}>No data available</td>
                </tr>
        );
    }
}

export default OrderPlaced;
