import React from 'react';

const CheckoutSuccess = () => {
    return (
        <div className="col-lg-10 col-md-10">
            <div style={{textAlign: 'center'}}>
                <h1>
                    Thank you!
                </h1>
                <h6>We received your order and will deliver it on time!</h6>
            </div>
        </div>
    );
};

export default CheckoutSuccess;
