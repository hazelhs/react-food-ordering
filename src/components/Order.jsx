import React, { Component } from 'react';
import {Link} from "react-router-dom";


class Order extends Component {

    render() {

        const totalItems = this.props.totalItems;

        return (

            <div className="col-lg-10 col-md-10 row">
                <div className="col-10">
                    <h1>Review Order</h1>
                    {totalItems > 0 ? <h6>You have ordered {this.props.totalItems} items.</h6> : ''}
                </div>
                {
                    totalItems > 0 ?
                        this.renderOrderList() :
                        <div className="col-lg-10">
                            <h3>You have not selected any food yet.</h3>
                        </div>
                }
            </div>

        );
    }

    renderOrderList() {
        return (
            <React.Fragment>

                <table className="table">
                    <thead>
                    <tr>
                        <th width="20%"></th>
                        <th width="30%">Menu</th>
                        <th width="20%">Quantity</th>
                        <th width="20%">Price</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.renderOrder()}

                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <div className="row">
                                <h4>
                                    Total:
                                </h4>
                            </div>
                        </td>
                        <td>
                            <div className="row">
                                <h4> ${this.calcTotalPrice()}</h4>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <div className="row">
                                <Link to="/menu" type="button" className="btn btn-secondary">
                                    <span className="fa fa-shopping-bag"></span> Continue Order
                                </Link>
                            </div>
                        </td>
                        <td>
                            <div className="row">
                                <Link to="/checkout" type="button" className="btn btn-success">
                                    Checkout <span className="fa fa-play"></span>
                                </Link>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>

                {/*<div className="col-10"></div>*/}

                {/*<div className="col">*/}
                {/*<strong className="totalPrice-label">*/}
                {/*Total: &nbsp;&nbsp;*/}
                {/*<i className="fa fa-dollar-sign mr-1"></i>{this.calcTotalPrice()}*/}
                {/*</strong>*/}
                {/*</div>*/}

            </React.Fragment>
        );
    }

    renderOrder() {
        return (

            this.props.orders.map(order => (
                <tr key={order._id}>
                    <td>
                        <img className="orderList-img" alt="order-img"
                             src={order.imageUrl}/>
                    </td>
                    <td>
                        {order.name}
                        <i className="ml-2 fa fa-trash pointer-icon"
                           onClick={() => {this.props.onDeleteOrder(order)}}>
                        </i>
                    </td>
                    <td>
                        <div className="row">

                            <button className="btn btn-sm btn-secondary quantity-btn-minus"
                                    disabled={order.quantity === 1 ? "disabled" : ''}
                                    onClick={() => {this.props.onDecreaseOrder(order)}} >
                                <i className="fa fa-minus"></i>
                            </button>

                            <div className="col-2 quantity-text">
                                {order.quantity}
                            </div>

                            {/*<button className="btn btn-sm btn-secondary quantity-btn-plus"*/}
                            {/*onClick={() => {this.handleIncrease(order)}}>*/}
                            {/*<i className="fa fa-plus"></i>*/}
                            {/*</button>*/}

                            <button className="btn btn-sm btn-secondary quantity-btn-plus"
                                    onClick={() => {this.props.onIncreaseOrder(order)}}>
                                <i className="fa fa-plus"></i>
                            </button>
                        </div>
                    </td>
                    <td>
                        ${this.calcSingleItemPrice(order.price, order.quantity)}
                    </td>
                </tr>
            ))
        )
    }

    calcSingleItemPrice(price, quantity) {
        const total = price*quantity;
        return total;
    }

    calcTotalPrice() {
        let totalPrice = 0;
        this.props.orders.forEach(order => {
            totalPrice += this.calcSingleItemPrice(order.price, order.quantity);
        });

        return totalPrice;
    }
}

export default Order;
