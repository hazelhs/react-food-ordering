import React from 'react';
import {Redirect} from "react-router-dom";
import Joi from "joi-browser";
import Form from "../../common/Form";
import orderService from "../../service/orderService";


class CheckoutDetailsForm extends Form {

    state = {
        isSuccessCheckout: Boolean = false,
        isSubmit: Boolean = false,

        countries: ['New Zealand', 'Singapore'],
        selectedCountry: '',
        states: ['Wellington', 'Dunedin', 'Auckland', 'Timaru', 'Nelson', 'Singapore'],
        selectedState: '',

        errors: {},
        data: {
            firstName: '',
            lastName: '',
            email: '',
            add1: '',
            add2: '',
            country: '',
            state: '',
            zip: '',

            cardHolder: '',
            cardNumber: '',
            cardExp: '',
            cvv: ''
        }
    };


    dataSchema = {
        firstName: Joi.string().required().min(5).label('First Name'),
        lastName: Joi.string().required().label('Last Name'),
        email: Joi.string().required().email({ minDomainAtoms: 2 }).label('Email'),
        add1: Joi.string().required().label('Address'),
        add2: Joi.string(),
        country: Joi.string().label('Country'),
        state: Joi.string().required(),
        zip: Joi.number().required().label('Zip'),

        cardHolder: Joi.string().required().label('Card Holder Name'),
        cardNumber: Joi.number().required().label('Card Number'),
        cardExp: Joi.number().required().label('Card Expiry'),
        cvv: Joi.number().required().label('CVV'),
    };


    componentDidMount() {
        const countries = [...this.state.countries];
        this.setState({countries: ['Please Select', ...countries]});

        const states = [...this.state.states];
        this.setState({states: ['Please Select', ...states]});
    };

    doSubmit = async() => {
        try {
            this.setState({isSubmit: true});

            let order = {
                items: {
                    cart: this.props.itemsInCart,
                    totalPrice: this.props.totalPrice
                },
                shipping: this.state.data

            };

            // console.log('submit details > ', order);

            const result = await orderService.createNewOrder(order);
            if(result) {
                localStorage.removeItem("cart-id");
                this.setState({isSuccessCheckout: true});
            };

        } catch (e) {
            console.error(e);
        }
    }


    render() {

        if (this.state.isSuccessCheckout) {
            return <Redirect to='/checkoutSuccess' />
        }

        const { selectedCountry, selectedState, countries, states} = this.state;

        return (
            <div>
                <h4 className="mb-3">Billing address</h4>
                <form className="needs-validation">
                    <div className="row">
                        <div className="col-md-6 ">
                            {this.renderInput("firstName", "First Name", "text")}
                        </div>

                        <div className="col-md-6 ">
                            {this.renderInput("lastName", "Last Name", "text")}
                        </div>
                    </div>

                    <div className="mb-3">
                        {this.renderInput("email", "Email", "email")}
                    </div>

                    <div className="mb-3">
                        {this.renderInput("add1", "Address 1", "text")}
                    </div>

                    <div className="mb-3">
                        {this.renderInput("add2", "Address 2", "text")}
                    </div>

                    <div className="row">

                        <div className="col-md-5 mb-3">
                            {this.renderSelect("country", "Country", countries, selectedCountry)}
                        </div>

                        <div className="col-md-4 mb-3">
                            {this.renderSelect("state", "State", states, selectedState)}
                        </div>

                        <div className="col-md-3 mb-3">
                            {this.renderInput("zip", "Zip", "text")}
                        </div>
                    </div>

                    <hr className="mb-4"/>

                    <h4 className="mb-3">Payment</h4>

                    <div className="row">
                        <div className="col-md-6 mb-3">
                            {this.renderInput("cardHolder", "Name on card", "text")}
                        </div>

                        <div className="col-md-6 mb-3">
                            {this.renderInput("cardNumber", "Credit card number", "text")}
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-3 mb-3">
                            {this.renderInput("cardExp", "Expiration", "text")}
                        </div>

                        <div className="col-md-3 mb-3">
                            {this.renderInput("cvv", "CVV", "text")}
                        </div>
                    </div>

                    <hr className="mb-4"/>

                    <button onClick={this.handleSubmit}
                            disabled={this.validate() || this.state.isSubmit ? "disabled" : ''}
                            className="btn btn-lg btn-primary btn-block" >
                        Continue to submit
                    </button>
                </form>
            </div>
        );
    }
}

export default CheckoutDetailsForm;
