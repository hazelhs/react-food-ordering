import React, {Component} from 'react';

import CheckoutCart from "./CheckoutCart";
import CheckoutDetailsForm from "./CheckoutDetailsForm";
import cartService from "../../service/foodCartService";

class Checkout extends Component {

    state ={
        itemsInCart: [],
        totalItems: 0,
        totalPrice: 0
    }

    async componentDidMount() {
        const result = await cartService.getItemsInCart();
        if(result) this.setState({itemsInCart: result.data.items});
        // console.log("checkout page items in cart > ", result.data.items);

        const totalItems = await cartService.getCartTotalItem();
        this.setState({totalItems});

        this.calcTotalPrice();
    }

    calcTotalPrice() {
        let total = 0;
        this.state.itemsInCart.forEach(item => {
            total += item.price * item.quantity;
        });
        this.setState({totalPrice: total});
    }

    render() {

        const {totalPrice, itemsInCart, totalItems} = this.state;

        return (

            <div className="col-lg-10 col-md-10 card checkout-card">
                <div className="card-header">
                    <h1>Checkout</h1>
                </div>

                <div className="card-body row">

                    <div className="col-md-4 order-md-2 mb-4">
                        <CheckoutCart
                            totalPrice={totalPrice}
                            itemsInCart={itemsInCart}
                            totalItems={totalItems}/>
                    </div>

                    <div className="col-md-8 order-md-1">
                        <CheckoutDetailsForm
                            itemsInCart={itemsInCart}
                            totalPrice={totalPrice}/>
                    </div>

                </div>
            </div>
        );
    }
}

export default Checkout;
