import React from 'react';
import Form from '../../common/Form';
import Joi from 'joi-browser';
import menuService from '../../service/menuService';
import categoryService from '../../service/categoryService';

class EditFood extends Form {

    state = {
        data: {
            _id: '',
            name: '',
            imageUrl: '',
            price: '',
            category: ''
        },
        errors: {},
        categories: [],
        selectedItem: {}
    };

    dataSchema = {
        _id: Joi.string(),
        name: Joi.string().required().min(3).label('Name'),
        price: Joi.string().required(),
        category: Joi.string().required(),
        imageUrl: Joi.string().required()
    };


    async getFoodCategories() {
        //get distinct categories
        let result = await categoryService.getMenuCategories();
        let categories = [];

        result.data.forEach( category => {
            categories.push(category);
        });
        return categories;
    }

    async mapMenuToModel() {
        const {data} = await menuService.getMenu(this.props.match.params.id);
        let menu = {
            _id: data._id,
            name: data.name,
            imageUrl: data.imageUrl,
            price: data.price,
            category: data.category
        };
        return menu;
    }

    async componentDidMount() {
        const data = await this.mapMenuToModel();
        const categories = await this.getFoodCategories();

        const selectedItem = data['category'];
        this.setState( {data, categories, selectedItem} );

    };

    doSubmit = async () => {
        const {data} = await menuService.updateMenu(this.state.data);
        // console.log("edited food >> ", data);

        this.props.history.push('/manageMenu');
    }



    render() {

        const {data, categories, selectedItem} = this.state;

        return (
            <React.Fragment>
                <div className="col-lg-10 col-md-10">

                    <h1>Edit Menu: {data.name}</h1>

                    <div className="row">
                        <div className="col-6">
                            <form>
                                {this.renderInput("name", "Name", "text")}

                                {/* {this.renderInput("category", "Category", "text")} */}

                                {this.renderSelect("category", "Category",
                                    categories,
                                    selectedItem)}


                                {/*{this.renderInput("price", "Price", "text")}*/}

                                {this.renderInputGroup("price", "Price")}

                                {this.renderInput("imageUrl", "Image URL", "text")}
                            </form>

                            {this.renderButton('Save')}
                        </div>

                        <div className="col">
                            <div className="card edit-menu-img">
                                <img className="card-img-top" src={data.imageUrl} alt=""></img>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default EditFood;
