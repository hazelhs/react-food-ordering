import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import menuService from '../../service/menuService';

import _ from 'lodash';
import Pagination from "../../common/Pagination";
import TableLengthFilterWrapper from "../../common/TableLengthFilterWrapper";
import TableHeader from "../../common/TableHeader";

class ManageFood extends Component {

    state = {
        menus: [],
        pageLengthArr: [5, 10, 15],
        selectedLength: 5,
        pageNum: 1,
        totalItems: '',
        totalPages: '',
        pages: [],
        sortColumn: {},
        searchInput: ''
    }

    async componentDidMount() {
        const {data} = await menuService.getMenus();
        this.setState({totalItems: data.length});

        await this.renderData();
    }


    async renderData() {
        this.computingPages();
        await this.getItemListWithQuery();
    }

    async getItemListWithQuery() {

        try {
            const {data} = await menuService.getMenusWithQuery(
                this.state.pageNum,
                this.state.selectedLength);

            this.setState({menus: data});
        } catch (e) {
            console.log('error in getting food list >> ', e);
        }
    }

    computingPages() {
        const totalPages = Math.ceil(this.state.totalItems / this.state.selectedLength);
        const pages = _.range(1, totalPages + 1);
        this.setState({pages, totalPages});
    }


    handleDelete = async (id) => {
        console.log("ID to delete ", id);
        await menuService.deleteMenu(id);

        await this.renderData();
    };

    handleSelectPageLength =  (e) => {
        this.setState({selectedLength: e.currentTarget.value});

        setTimeout(async () => {
            this.setState({pageNum:1});
            await this.renderData();
        }, 100);
    };

    handlePageChange = page => {
        // console.log('page ', page);

        if(page === '-1') {
            if(this.state.pageNum > 1) {
                this.setState({pageNum: this.state.pageNum-1});
            }
        } else if ( page === '+1') {
            if(this.state.pageNum !== this.state.totalPages) {
                this.setState({pageNum: this.state.pageNum + 1});
            }
        } else {
            this.setState({pageNum: page});
        }

        setTimeout(async () => {
            await this.renderData();
        }, 200);

    };

    onSearchInputChange = e => {
        // console.log(e.currentTarget.value);
        this.setState({searchInput: e.currentTarget.value});
    };

    handleSearchMenu = async () => {
        // console.log('get search input and call to backend...');
        const {data} = await menuService.searchMenuByName(this.state.searchInput);
        this.setState({menus: data, totalItems: data.length});

        this.computingPages();
        this.setState({searchInput: ""});
    }

    handleSort = path => {
        // console.log(path);
        this.setState({sortColumn: {path, order: 'asc'}});
    }


    render() {
        const { location } = this.props;
        const {menus, pageLengthArr, selectedLength, pageNum, pages, totalItems, searchInput} = this.state;

        return (

            <div className="col-lg-10 col-md-10">

                <div className="card">
                    <TableHeader
                        isAddBtnExist={true}
                        currentItems={menus.length}
                        totalItems={totalItems}
                        currentPath={location.pathname}/>

                    <div className="card-body">
                        <TableLengthFilterWrapper
                            searchInput={searchInput}
                            selectedLength={selectedLength}
                            pageLengthArr={pageLengthArr}
                            onSelectPageLength={this.handleSelectPageLength}
                            onInputChange={this.onSearchInputChange}
                            onSearchInput={this.handleSearchMenu}/>

                        <table className="table-responsive-md table table-bordered table-striped col" >
                            <thead>
                            <tr>
                                <th width="30%" onClick={() => this.handleSort('name')}>Name</th>
                                <th width="20%" onClick={() => this.handleSort('category')}>Category</th>
                                <th width="10%" onClick={() => this.handleSort('price')}>Price</th>
                                <th width="10%"></th>
                            </tr>
                            </thead>

                            <tbody>
                            {this.renderMenus()}
                            </tbody>
                        </table>

                        <Pagination
                            pageNum={pageNum}
                            pages={pages}
                            onPageChange={this.handlePageChange} />
                    </div>
                </div>
            </div>

        );
    }

    H
    renderMenus() {
        return  (
            this.state.menus.length > 0
                ? this.state.menus.map(menu => (
                    <tr key={menu._id} >
                        <td>{menu.name}</td>
                        <td>{menu.category}</td>
                        <td>$ {menu.price}</td>
                        <td>
                            <Link className="btn btn-sm btn-primary m-2"
                                  to={`${this.props.location.pathname}/${menu._id}`}>
                                <i className="fa fa-pencil"></i>
                            </Link>
                            <button className="btn btn-sm btn-danger m-2"
                                    onClick={() => {this.handleDelete(menu._id)}} >
                                <i className="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                ))
                : <tr>
                    <td colSpan="4" style={{'textAlign': 'center'}}>No data available</td>
                </tr>

        );
    }
}

export default ManageFood;
