import React from 'react';

import Joi from 'joi-browser';
import Form from '../../common/Form';
import menuService from '../../service/menuService';
import categoryService from '../../service/categoryService';


// class AddFood extends Component {
class AddFood extends Form {
    state = {
        selectedItem: '',
        data: {
            name: '',
            price: '',
            category: '',
            imageUrl: ''
        },
        errors: {},
        categories: []

    }

    dataSchema = {
        name: Joi.string().required().min(3).label('Name'),
        price: Joi.number().required().label('Price'),
        category: Joi.string().required(),
        imageUrl: Joi.string().required()
    }

    async componentDidMount() {
        let categories = await this.getFoodCategories();
        categories = ['Please select a category', ...categories]
        this.setState( {categories} );
    };

    async getFoodCategories() {
        //get distinct categories
        let result = await categoryService.getMenuCategories();

        let categories = [];
        result.data.forEach( category => {
            categories.push(category);
        });

        console.log('categories ', categories)
        return categories;
    }

    doSubmit = async () => {

        try {
            const {data} =  await menuService.addMenu(this.state.data);

            // this.props.history.push(`${this.props.location.path}`);
            // console.log(this.props.location.path);
            this.props.history.push("/manageMenu");

        }  catch (ex) {
            //Expected error (404: not found, 400: bad request) - client errors
            // - display a specific error message

            // Unexpected error (network down, server down, db down, bug)
            // log them,  display a generic and friendly error message
            if(ex.response && ex.response.status === 404)
                alert('This menu ...');

        }
    }

    render() {
        const { categories, selectedItem } = this.state;

        return (

            <div className="col-lg-10 col-md-10">
                <h1>Add Menu</h1>

                <div className="col-8">
                    <form>
                        {this.renderInput("name", "Name", "text")}

                        {/* {this.renderInput("category", "Category", "text")} */}

                        {this.renderSelect("category", "Category",
                            categories,
                            selectedItem )}

                        {/* {this.renderInput("price", "Price", "text")} */}

                        {this.renderInputGroup("price", "Price")}

                        {this.renderInput("imageUrl", "Image URL", "text")}


                        {/* { <Input
                                name="name"
                                label="Name"
                                value={data.name}
                                type="text"
                                error={errors.name}
                                onChange={this.handleInputChange} /> */}

                    </form>

                    {this.renderButton('Save')}

                    {/* <button disabled={this.validate()}
                                className="btn btn-primary m-3" onClick={this.handleSubmit} >Add
                        </button> */}

                </div>

            </div>

        );
    }
}

export default AddFood;
