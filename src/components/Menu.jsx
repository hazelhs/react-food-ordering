import React, { Component } from 'react';
import menuService from '../service/menuService';
import catalogService from '../service/categoryService';
import ListGroup from '../common/ListGroup';


class Menu extends Component {

    state = {
        catalogs: [],
        categories: [{
            id: '',
            name: ''
        }],
        selectedCategory: {}
    }


    async componentDidMount() {

        const menus = await menuService.getMenus();
        const categories = await this.getFoodCategories();
        const selectedCategory = {  id: '',name: 'All Categories'};

        this.setState( { catalogs: menus.data, categories, selectedCategory} );
    }

    async getFoodCategories() {
        //get distinct categories
        let result = await catalogService.getMenuCategories();
        let list = [];

        result.data.forEach( (c, index) => {
            let category = {
                id: index,
                name: c
            };
            list.push(category);
        });
        return [{ id: '',name: 'All Categories'}, ...list ];
    }


    handleSelection = async (category) => {
        // console.log(category);
        this.setState( {selectedCategory: category} );

        if(category.id === "") {
            const result = await menuService.getMenus();
            this.setState({catalogs: result.data });
        } else {
            const result = await menuService.filterByCategory(category.name);
            this.setState({catalogs: result.data });
        }
    };


    render() {
        const { categories, selectedCategory, catalogs, } = this.state;
        const { orders, totalItems} = this.props;

        // console.log('added orders > ', orders);
        // console.log('total items > ', totalItems)

        return (

            <div className="col-lg-10">
                <div className="row">

                    <div className="col-lg-3 my-4">
                        <ListGroup categories={categories}
                                   selectedCategory={selectedCategory}
                                   onClick={this.handleSelection} />
                    </div>


                    <div className="col-lg-9">
                        <div className="row my-4">
                            { catalogs && catalogs.map( (catalog, index) => (
                                <div className="col-lg-4 col-md-6 mb-4" key={index}>
                                    <div className="card h-100">
                                        <img className="card-img-top" style={{'height' : '180px'}}
                                             src={catalog.imageUrl} href={catalog.imageUrl} />
                                        <div className="card-body">
                                            <h4 className="card-title">
                                                {catalog.name}
                                            </h4>
                                            <h5>${catalog.price}</h5>
                                            <p className="card-text">
                                                {catalog.description}
                                            </p>
                                        </div>
                                        {/*<div className="card-footer">*/}
                                        {/*{orders.length > 0*/}
                                        {/*? orders.indexOf(catalog) > -1*/}
                                        {/*?  <a className="btn btn-primary btn-block pointer-icon menu-footer-button"*/}
                                        {/*onClick={ () => this.props.onAddOrder(catalog) }>*/}
                                        {/*Add Order*/}
                                        {/*</a>*/}
                                        {/*:  <a className="btn btn-secondary btn-block menu-footer-button">*/}
                                        {/*Added*/}
                                        {/*</a>*/}
                                        {/*:  <a className="btn btn-primary btn-block pointer-icon menu-footer-button"*/}
                                        {/*onClick={ () => this.props.onAddOrder(catalog) }>*/}
                                        {/*Add Order*/}
                                        {/*</a>}*/}
                                        {/*/!*<i className="fal fa-2x fa-thumbs-up"></i>*!/*/}
                                        {/*</div>*/}



                                        <div className="card-footer">
                                            {orders.length > 0
                                                ? orders.findIndex(order => order._id === catalog._id) > -1
                                                    ?  <a className="btn btn-secondary btn-block
                                                                      menu-footer-button">
                                                        Added
                                                    </a>
                                                    :   <a className="btn btn-primary btn-block
                                                                       pointer-icon menu-footer-button"
                                                           onClick={ () => this.props.onAddOrder(catalog) }>
                                                        Add Order
                                                    </a>
                                                :  <a className="btn btn-primary btn-block
                                                                pointer-icon menu-footer-button"
                                                      onClick={ () => this.props.onAddOrder(catalog) }>
                                                    Add Order
                                                </a>}
                                            {/*<i className="fal fa-2x fa-thumbs-up"></i>*/}
                                        </div>
                                    </div>
                                </div>
                            )) }
                        </div>
                    </div>

                </div>

            </div>


        );
    }
}

export default Menu;
