import React from 'react';

const Pagination = ({pages, pageNum, onPageChange}) => {
    return (
        <nav>
            <ul className="pagination justify-content-end pointer-icon">
                <li className="page-item" onClick={ () => onPageChange('-1')}>
                    <a className="page-link">
                        <span>&laquo;</span>
                        <span className="sr-only">Previous</span>
                    </a>
                </li>
                {pages.map( page => (
                    <li key={page}
                        className={ page === pageNum  ? "page-item active" : "page-item"}
                        onClick={() => onPageChange(page)}>
                        <a className="page-link">{page}</a>
                    </li>
                ))}
                {/*<li className="page-item" onClick={onClickNextPage}>*/}
                <li className="page-item" onClick={ () => onPageChange('+1')}>
                    <a className="page-link">
                        <span>&raquo;</span>
                        <span className="sr-only">Next</span>
                    </a>
                </li>
            </ul>
        </nav>
    );
};

export default Pagination;
