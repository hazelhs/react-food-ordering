import React, {Component} from 'react';
import {Link} from "react-router-dom";

class TableHeader extends Component {
    render() {

        const {totalItems, currentItems, currentPath, isAddBtnExist} = this.props;

        return (
            <div className="card-header table-header">
                <h4 className="card-header-title">
                    Showing {currentItems} out of {totalItems} items
                </h4>

                {isAddBtnExist
                    ? <div>
                        <Link className="btn btn-sm btn-primary card-header-button"
                              to={`${currentPath}/add`}>
                            Add
                        </Link>
                    </div>
                    : ''
                }
            </div>
        );
    }
}

export default TableHeader;
