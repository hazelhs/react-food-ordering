import React, { Component } from 'react';
import Joi from 'joi-browser';
import Input from './Input';
import Select from "./Select";


class Form extends Component {

    state = { 
        data: {},
        errors: {}
    };

    
    validate = () => {
        const result = Joi.validate(this.state.data, this.dataSchema, {abortEarly: false});
        // console.log(result)
        if(!result.error) return null;

        const errors = {};
        result.error.details.forEach(err => {
            errors[err.path[0]] = err.message;
        });

        return errors;
    }

    validateProperty = ( {name, value} ) => {

        // console.log(name, value)
        const obj = { [name]: value };
        const schema = { [name]: this.dataSchema[name] };
        const {error} = Joi.validate(obj, schema, {abortEarly: false});
        // console.log({error});

        return error ? error.details[0].message : null;
    }


    handleSubmit = e => {
        e.preventDefault();

        const errors = this.validate();
        // console.log(errors);

        this.setState( {errors: errors || {} });
        if(errors) return;

    
        this.doSubmit();
    }


    handleInputChange = e => {
        const errors = {...this.state.errors};
        const errorMsg = this.validateProperty(e.currentTarget);
    
        if(errorMsg) {
            errors[e.currentTarget.name] = errorMsg;
            this.setState({errors})
        }
          
        else {
            delete errors[e.currentTarget.name];
            this.setState({errors})
        }

        const data = {...this.state.data};
        data[e.currentTarget.name] = e.currentTarget.value;
        this.setState( {data} );
    };

    handleSelectChange = e => {
        // console.log('name ', e.currentTarget.name + 'value ', e.currentTarget.value);

        if(e.currentTarget.name === 'country') {
            this.setState({selectedCountry: e.currentTarget.value});
        }
        else if (e.currentTarget.name === 'state') {
            this.setState({selectedState: e.currentTarget.value});
        }


        const data = {...this.state.data};
        data[e.currentTarget.name] = e.currentTarget.value;
        this.setState( {data} );
    }


    renderButton(label) {
        return (
          <button disabled={this.validate()} className="btn btn-primary mb-3"
                    onClick={this.handleSubmit}>{label}</button>
        )
    }
      

    renderInput( name, label, type) {

        const {data, errors} = this.state;

        return (
            <Input 
                name={name} 
                label={label}
                value={ data[name] }
                type={type}
                error={errors[name]}
                onChange={this.handleInputChange} />
        )
    }

    renderInputGroup(name, label) {

        const {data, errors} = this.state;
       
        return (
            <div className="form-group"> 
                <label htmlFor={name}>{label}</label>
                <div className="input-group">
                    <div className="input-group-prepend">
                        <span className="input-group-text">
                            <i className="fa fa-dollar"></i>
                        </span>
                    </div>
                    <input type="text" className="form-control" 
                            value={data[name]} name={name}
                            onChange={this.handleInputChange}
                            placeholder="20.00" />
                </div>

                {errors[name] && <div className="alert alert-danger">{errors[name]}</div>}
            </div>
        )
    }

    renderSelect( name, label, list, selectedItem) {

        return (
            <Select name={name}
                    label={label}
                    selections={list}
                    selectedItem={selectedItem}
                    onSelectChange={this.handleSelectChange}/>


          /*  <div className="form-group" >
                <label htmlFor={name}>{label}</label>
                <select className="form-control"
                    onChange={this.handleSelectChange}
                    value={selectedItem}
                    id={name} name={name}>
                        {list.map( (item, index) => (
                            <option value={item} key={index}>
                                {item}
                            </option>
                        )) }
                </select>
            </div>
*/

        )
    }
}

export default Form;
