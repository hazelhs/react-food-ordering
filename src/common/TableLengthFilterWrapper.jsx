import React from 'react';

const TableLengthFilterWrapper = ({selectedLength, pageLengthArr, onSelectPageLength,
                                      searchInput, onInputChange, onSearchInput}) => {
    return (
        <div className="table-header mb-3">
            <div>
                <label htmlFor="pageSize" className="mr-2">Shows</label>

                <select className="form-control select-paging mr-2"
                    value={selectedLength} id="pageSize" name="pageSize"
                    onChange={onSelectPageLength}>
                    {pageLengthArr.map(item => (
                        <option value={item} key={item}>
                            {item}
                        </option>
                    )) }
                </select>

                <label>entries</label>
            </div>

            <div>
                <div className="input-group">
                    <input type="text" value={searchInput} id='searchMenuInput'
                           placeholder="Enter menu name"
                           className="form-control"
                           onChange={onInputChange} />
                    <div className="input-group-append">
                       <span className="input-group-text"
                            onClick={onSearchInput}>
                           <i className="fa fa-search"></i>
                       </span>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default TableLengthFilterWrapper;

