import http from "./httpService";

const apiEndpoint = "/orders";

export async function createNewOrder(checkoutDetails) {
    return await http.post(apiEndpoint, checkoutDetails);
};

export async function getOrders() {
    return await http.get(apiEndpoint);
}

export default {
    createNewOrder,
    getOrders
}
