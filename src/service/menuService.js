// import axios from 'axios';
import http from './httpService';

const apiEndpoint = "/menus";


export async function getMenu(id) {
    const result = await http.get(apiEndpoint + '/' + id);
    return result;
};

export async function getMenus() {
    const result = await http.get(apiEndpoint);
    return result;
};


export async function getMenusWithQuery(pageNum, itemPerPage) {
    // console.log(pageNum);
    // console.log(itemPerPage)

    const queryString = "?page=" + pageNum + "&limit=" + itemPerPage;
    const result = await http.get(apiEndpoint + queryString );
    return result;
};



export async function addMenu(menu) {
    const result = await http.post(apiEndpoint, menu);
    return result;
};

export async function updateMenu(menu) {
    const body = {...menu};
    delete body._id;
    const result = await http.put(apiEndpoint + "/" + menu._id, body);
    return result;
};

export async function deleteMenu(menuId) {
    const result = await http.delete(apiEndpoint + "/" + menuId);
    return result;
};

export async function filterByCategory(category) {
    const result = await http.get(apiEndpoint + "?category=" + category);
    return result;
};

export async function searchMenuByName(menuName) {
    const result = await http.get(apiEndpoint + "?searchByName=" + menuName);
    return result;
}



export default {
    getMenu,
    getMenus,
    addMenu,
    updateMenu,
    deleteMenu,
    filterByCategory,

    getMenusWithQuery,
    searchMenuByName
};
